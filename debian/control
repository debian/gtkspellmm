Source: gtkspellmm
Section: libs
Priority: optional
Maintainer: Philip Rinn <rinni@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 doxygen,
 graphviz,
 libgtkmm-3.0-dev (>= 3.16.0-2),
 libgtkspell3-3-dev (>= 3.0.9),
 libtool,
 mm-common,
 xsltproc,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://gtkspell.sourceforge.net
Vcs-Git: https://salsa.debian.org/debian/gtkspellmm.git
Vcs-Browser: https://salsa.debian.org/debian/gtkspellmm

Package: libgtkspellmm-3.0-0v5
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Conflicts:
 libgtkspellmm-3.0-0,
Replaces:
 libgtkspellmm-3.0-0,
Description: C++ wrapper library for GtkSpell (shared libraries)
 GtkSpellmm provides C++ bindings for the GtkSpell spell-checking library.
 GtkSpell provides LibreOffice-style highlighting of misspelled words in a
 GtkTextView widget. Right-clicking a misspelled word pops up a menu of
 suggested replacements.
 .
 This package contains the shared libraries.

Package: libgtkspellmm-3.0-dev
Section: libdevel
Architecture: any
Depends:
 libgtkmm-3.0-dev (>= 3.16.0-2),
 libgtkspell3-3-dev (>= 3.0.9),
 libgtkspellmm-3.0-0v5 (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 libgtkspellmm-3.0-doc,
Multi-Arch: same
Description: C++ wrapper library for GtkSpell (development files)
 GtkSpellmm provides C++ bindings for the GtkSpell spell-checking library.
 GtkSpell provides LibreOffice-style highlighting of misspelled words in a
 GtkTextView widget. Right-clicking a misspelled word pops up a menu of
 suggested replacements.
 .
 This package contains the development libraries and header
 files needed by C++ programs that want to compile with GtkSpell.

Package: libgtkspellmm-3.0-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Suggests:
 www-browser,
Description: C++ wrappers for GtkSpell (documentation)
 GtkSpellmm provides C++ bindings for the GtkSpell spell-checking library.
 GtkSpell provides LibreOffice-style highlighting of misspelled words in a
 GtkTextView widget. Right-clicking a misspelled word pops up a menu of
 suggested replacements.
 .
 This package contains the HTML reference documentation.
